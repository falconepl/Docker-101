FROM ubuntu:16.04

CMD echo "Hello Aga!"

COPY ./first.sh /home/first.sh

COPY ./second.sh /home/second.sh

ENTRYPOINT ["/home/first.sh", "Bazinga!"]
