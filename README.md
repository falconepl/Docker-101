
README
======

# Howto

```
$ cd <THIS_DIRECTORY>
$ docker build --tag ao_image .
$ docker images
$ docker run --detach --name ao_container ao_image
$ docker ps --all
$ docker logs ao_container
```

## Notes:

* Note the dot ('.') at the end of 'docker build' command
* ao_image - name of the image
* ao_container - name of the container
